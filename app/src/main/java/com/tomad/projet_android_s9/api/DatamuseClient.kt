package com.tomad.projet_android_s9.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface DatamuseClient {

    @GET("words") // Means Like
    fun synonyms(@Query("ml") expression: String, @Query("max") max: Int = 10) : Call<List<Entry>>

    @GET("words") // Means Like
    fun homonyms(@Query("sl") expression: String, @Query("max") max: Int = 10) : Call<List<Entry>>

    @GET("sug") // Means Like
    fun suggestions(@Query("s") expression: String, @Query("max") max: Int = 3) : Call<List<Entry>>

}