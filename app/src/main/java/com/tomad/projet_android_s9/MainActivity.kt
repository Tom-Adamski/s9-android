package com.tomad.projet_android_s9

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.tomad.projet_android_s9.adapter.AutoSuggestAdapter
import com.tomad.projet_android_s9.api.DatamuseClient
import com.tomad.projet_android_s9.api.Entry
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    private var client: DatamuseClient

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.datamuse.com")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        client = retrofit.create(DatamuseClient::class.java)
    }

    private val spinnerModes = arrayOf(
        "Means like", "Sounds like"
    )

    private lateinit var autoSuggestAdapter: AutoSuggestAdapter
    private lateinit var suggestionHandler: Handler


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val spinnerAdapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_list_item_1,
            android.R.id.text1,
            spinnerModes
        )
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mode_spinner.adapter = spinnerAdapter

        autoSuggestAdapter = AutoSuggestAdapter(this, android.R.layout.simple_dropdown_item_1line)

        queryEditText.threshold = 2
        queryEditText.setAdapter(autoSuggestAdapter)

        suggestionHandler = Handler(
            object : Handler.Callback {
                override fun handleMessage(msg: Message): Boolean {
                    if (msg.what == TRIGGER_AUTO_COMPLETE) {
                        if (!TextUtils.isEmpty(queryEditText.text.toString())) {
                            fetchSuggestions(queryEditText.text.toString())
                        }
                    }
                    return false
                }
            }
        )

        queryEditText.addTextChangedListener(
            object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    suggestionHandler.removeMessages(TRIGGER_AUTO_COMPLETE)
                    suggestionHandler.sendEmptyMessageDelayed(
                        TRIGGER_AUTO_COMPLETE,
                        AUTO_COMPLETE_DELAY
                    )
                }

                override fun afterTextChanged(p0: Editable?) {

                }
            }

        )
    }

    fun fetchSuggestions(text: String) {
        val call = client.suggestions(text, 4)

        call.enqueue(object : Callback<List<Entry>> {
            override fun onResponse(call: Call<List<Entry>>, response: Response<List<Entry>>) {

                val results = ArrayList<String>()

                response.body()?.forEach {
                    results.add(it.word)
                }

                autoSuggestAdapter.setData(results)
                autoSuggestAdapter.notifyDataSetChanged()

            }

            override fun onFailure(call: Call<List<Entry>>, t: Throwable) {

            }
        })


    }

    fun search(v: View) {

        val position = mode_spinner.selectedItemPosition

        val call = when (spinnerModes[position]) {
            "Means like" -> client.synonyms(queryEditText.text.toString())
            "Sounds like" -> client.homonyms(queryEditText.text.toString())


            else -> client.synonyms(queryEditText.text.toString())
        }


        call.enqueue(object : Callback<List<Entry>> {
            override fun onResponse(call: Call<List<Entry>>, response: Response<List<Entry>>) {

                val results = ArrayList<String>()

                response.body()?.forEach {
                    results.add(it.word)
                }

                val adapter = ArrayAdapter<String>(
                    this@MainActivity,
                    android.R.layout.simple_list_item_1,
                    android.R.id.text1,
                    results
                )
                result_list.adapter = adapter


            }

            override fun onFailure(call: Call<List<Entry>>, t: Throwable) {
                //TODO toast
            }
        })
    }

    companion object {
        const val TRIGGER_AUTO_COMPLETE = 100
        const val AUTO_COMPLETE_DELAY: Long = 300
    }

}
